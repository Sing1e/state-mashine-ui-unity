﻿using System;
using System.Collections;
using System.Collections.Generic;
using Pixeye.Framework;
using UniRx;
using UnityEngine;

public class ProcessorCounter : Processor
{
    public ReactiveProperty<int> count = new ReactiveProperty<int>(0);
    public ProcessorCounter()
    {
        Observable.Timer(TimeSpan.FromSeconds(1))
            .Repeat()
            .Subscribe(_ => { count.Value += 1; });
    }
}
