﻿using System;
using Pixeye.Framework;
using UnityEngine;

public class ProcessorUI : Processor, ITick
{
    public static MenuManager MenuManager;

    public ProcessorUI()
    {
        var canvas = GameObject.Find("Canvas").transform;
        foreach (var aChild in canvas.GetComponentsInChildren<IUIShowableHidable>(true))
        {
            aChild.Instantiate();
        }
        MenuManager = new MenuManager(
            new MainMenuState(MainMenuUI.Instance),
            new SettingMenuState(SettingsMenuUI.Instance),
            new MessageBoxState(MessageBoxUI.Instance));
        
        
        MenuManager.GoToScreenOfType<MainMenuState>(
            new Action(()=>{Debug.Log("StartGame");}),
            new Action(() =>
            {
                OpenSetting();
            }),
            new Action(()=>{Debug.Log("ExitGame");})
        );
        
    }
    
    void OpenSetting()
    {
        MenuManager.GoToScreenOfType<SettingMenuState>();
    }

    public void Tick()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            MenuManager.GoToPreviousScreen();
        }
    }
}
