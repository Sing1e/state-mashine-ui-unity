﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuState : UIState
{
    protected override IUIShowableHidable ShowableHidable { get; set; }
    private readonly MainMenuUI mainMenuUi;
    
    private Action onStartButtonClicked;
    private Action onSettingsButtonClicked;
    private Action onExitButtonClicked;
    
    public MainMenuState(MainMenuUI mainMenuUi)
    {
        ShowableHidable = mainMenuUi;
        this.mainMenuUi = mainMenuUi;
    }
    protected override void Enter(params object[] parameters)
    {
        onStartButtonClicked = (Action)parameters[0];
        onSettingsButtonClicked = (Action)parameters[1];
        onExitButtonClicked = (Action)parameters[2];

        mainMenuUi.onStartButtonClicked += onStartButtonClicked;
        mainMenuUi.onSettingsButtonClicked += onSettingsButtonClicked;
        mainMenuUi.onExitButtonClicked += onExitButtonClicked;
    }


    protected override void Exit()
    {
        mainMenuUi.onStartButtonClicked -= onStartButtonClicked;
        mainMenuUi.onSettingsButtonClicked -= onSettingsButtonClicked;
        mainMenuUi.onExitButtonClicked -= onExitButtonClicked;
    }
}
