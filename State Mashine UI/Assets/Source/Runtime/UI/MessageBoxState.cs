﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageBoxState : UIState
{
    protected override IUIShowableHidable ShowableHidable { get; set; }
    private readonly MessageBoxUI messageBoxUi;
    private Action onOkButtonClicked;
    

    public MessageBoxState(MessageBoxUI messageBoxUi)
    {
        ShowableHidable = messageBoxUi;
        this.messageBoxUi = messageBoxUi;
    }
    protected override void Enter(params object[] parameters)
    {
        messageBoxUi.SetMessage(parameters[0].ToString());
        onOkButtonClicked = (Action)parameters[1];
        messageBoxUi.onOkButtonClicked += onOkButtonClicked;
    }

    protected override void Exit()
    {
        messageBoxUi.onOkButtonClicked -= onOkButtonClicked;
    }
}
