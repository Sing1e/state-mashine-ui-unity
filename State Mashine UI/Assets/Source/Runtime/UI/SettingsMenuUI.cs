﻿using System;
using Pixeye.Framework;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SettingsMenuUI : UIShowableHidable
{
    public static SettingsMenuUI Instance { get; private set; }

    public Action showAnim;
    public Action hideAnim;
    
    [SerializeField]
    private Button backButton;
    [SerializeField]
    private Button popApButton;
    
    public Text counter;
    public CompositeDisposable disposable;
    public Action onBackButtonClicked;
    public Action onPopApButtonClicked;

    protected override void Awake()
    {
        backButton.onClick.AddListener(() =>
        {
            if (onBackButtonClicked != null)
            {
                onBackButtonClicked();
            }
        });
        
        popApButton.onClick.AddListener(() =>
        {
            if (onPopApButtonClicked != null)
            {
                onPopApButtonClicked();
            }
        });
        
    }

    public override void Instantiate()
    {
        base.Instantiate();
        Instance = this;
        SetAnimation();

    }

    public void SubscriptionUi()
    {
        disposable = new CompositeDisposable();
        Toolbox.Get<ProcessorCounter>().count
            .ObserveEveryValueChanged(x => x.Value)
            .Subscribe(xs => { counter.text = xs.ToString(); }).AddTo(disposable);
    }

    public override void ShowUI()
    {
        if (showAnim != null)
        {
            showAnim();
        }
    }

    public override void HideUI()
    {
        if (hideAnim != null)
        {
            hideAnim();
        }
    }

    public void SetAnimation()
    {
        if (showAnim != null && hideAnim != null)
        {
            return;
        }

        showAnim = () =>
        {
            hide = true;
            rectTransform.anchoredPosition = new Vector2(1920, 0);
            base.ShowUI();
            rectTransform.DOAnchorPos(Vector2.zero, 0.25f, true);
        };

        hideAnim = () =>
        {
            hide = false;
            rectTransform.DOAnchorPos(new Vector2(1920, 0), 0.25f, true)
                .OnComplete(() => { base.HideUI(); });
        };
    }
}
