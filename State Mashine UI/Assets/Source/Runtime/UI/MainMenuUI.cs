﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuUI : UIShowableHidable
{
    public static MainMenuUI Instance { get; private set; }
    
    [SerializeField]
    private Button startButton;
    [SerializeField]
    private Button settingsButton;
    [SerializeField]
    private Button exitButton;
    
    public Action onStartButtonClicked;
    public Action onSettingsButtonClicked;
    public Action onExitButtonClicked;

    

    protected override void Awake()
    {
           
        startButton.onClick.AddListener(() =>
        {
            if (onStartButtonClicked != null)
            {
                onStartButtonClicked();
            }
        });
        settingsButton.onClick.AddListener(() =>
        {
            if (onSettingsButtonClicked != null)
            {
                onSettingsButtonClicked();
            }
        });
        exitButton.onClick.AddListener(() =>
        {
            if (onExitButtonClicked != null)
            {
                onExitButtonClicked();
            }
        });
    }
    public override void Instantiate()
    {
        base.Instantiate();
        Instance = this;
    }
    
    public override void ShowUI()
    {
        hide = true;
        base.ShowUI();
        rectTransform.DOAnchorPos(Vector2.zero, 0.25f, true);
    }

    public override void HideUI()
    {
        hide = false;
        rectTransform.DOAnchorPos(new Vector2(-750,0), 0.25f, true)
            .OnComplete(() =>
            {
                base.HideUI();
            });

    }
}
