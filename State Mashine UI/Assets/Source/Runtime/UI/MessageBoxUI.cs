﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class MessageBoxUI : UIShowableHidable
{
    public static MessageBoxUI Instance { get; private set; }
    
    [SerializeField]
    private Text messageText;
    [SerializeField] 
    private Button okButton;

    public Action onOkButtonClicked;


    protected override void Awake()
    {
        okButton.onClick.AddListener(() =>
        {
            if (onOkButtonClicked != null)
            {
                onOkButtonClicked();
            }
        });
    }

    public void SetMessage(string message)
    {
        messageText.text = message;
    }

    public override void Instantiate()
    {
        base.Instantiate();
        Instance = this;
    }

    public override void ShowUI()
    {
        rectTransform.localScale = new Vector3(0,0);
        base.ShowUI();
        rectTransform.DOScale(new Vector3(1,1), 0.25f);
    }

    public override void HideUI()
    {
        rectTransform.DOScale(new Vector3(0,0), 0.25f)
            .OnComplete(() =>
            {
                base.HideUI();
            });
    }
}
