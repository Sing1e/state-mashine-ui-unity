﻿using System;

public class SettingMenuState : UIState
{
    protected override IUIShowableHidable ShowableHidable { get; set; }    
    private readonly SettingsMenuUI settingsMenuUi;
    private Action onBackButtonClicked;
    private Action onPopApButtonClicked;

    public SettingMenuState(SettingsMenuUI settingsMenuUi)
    {
        ShowableHidable = settingsMenuUi;
        this.settingsMenuUi = settingsMenuUi;

        

        onPopApButtonClicked = () =>
        {
            settingsMenuUi.showAnim = null;
            settingsMenuUi.hideAnim = null;
            ProcessorUI.MenuManager.GoToScreenOfType<MessageBoxState>(
                "This is you message",
                new Action(() =>
                {
                    ProcessorUI.MenuManager.GoToPreviousScreen();
                }));
        };
    }
    
    protected override void Enter(params object[] parameters)
    {
        settingsMenuUi.SubscriptionUi();
        settingsMenuUi.SetAnimation();
        
        onBackButtonClicked = ()=>
        {
            ProcessorUI.MenuManager.GoToPreviousScreen();
        };

        settingsMenuUi.onBackButtonClicked += onBackButtonClicked;
        settingsMenuUi.onPopApButtonClicked += onPopApButtonClicked;


    }

    protected override void Exit()
    {
        settingsMenuUi.onBackButtonClicked -= onBackButtonClicked;
        settingsMenuUi.onPopApButtonClicked -= onPopApButtonClicked;

        if (settingsMenuUi.disposable != null)
        {
            settingsMenuUi.disposable.Dispose();
        }
    }
}
