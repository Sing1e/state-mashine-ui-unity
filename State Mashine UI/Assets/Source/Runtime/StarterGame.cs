﻿using System.Collections;
using System.Collections.Generic;
using Pixeye;
using Pixeye.Framework;
using UnityEngine;

public class StarterGame : Starter
{
    protected override void Setup()
    {
        Add<ProcessorUI>();
        Add<ProcessorCounter>();
    }
}
